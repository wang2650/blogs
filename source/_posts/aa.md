
---
title: 哔哩哔哩视频和网易云的使用
date: 2023-08-25 13:14:45
tags: [影音]
---
#### 介绍

##### 网易云的歌单，通过iframe的方式实现

##### 哔哩哔哩的视频通过引用js和css文件来实现。（不登录的情况下，画质比较低）
<!-- {% vjs2 "video=https://upos-sz-mirror08c.bilivideo.com/upgcxcode/12/27/1096942712/1096942712-1-16.mp4?e=ig8euxZM2rNcNbRVhwdVhwdlhWdVhwdVhoNvNC8BqJIzNbfq9rVEuxTEnE8L5F6VnEsSTx0vkX8fqJeYTj_lta53NCM=&uipk=5&nbs=1&deadline=1692948696&gen=playurlv2&os=08cbv&oi=17621919&trid=2e15e32b5f3e4166ad190c886f04ec5bh&mid=0&platform=html5&upsig=9eb320b95ed7be243d76b4c44098493c&uparams=e,uipk,nbs,deadline,gen,os,oi,trid,mid,platform&bvc=vod&nettype=0&f=h_0_0&bw=48608&logo=80000000" "aspect-ratio=16-9" "loop" "autoplay"  %} -->
<!-- <div style="position: relative; width: 100%; height: 0; padding-bottom: 75%;">
<iframe src="//player.bilibili.com/player.html?aid=442601182&bvid=BV11L411e7j8&cid=1096942712&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"   style="position:absolute; height: 100%; width: 100%;"> </iframe></div> -->

<div id="video" style="width: 100%; height: 400px;max-width: 600px;"></div>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/p2p-ckplayer@latest/ckplayer/ckplayer.min.js" charset="UTF-8"></script>
<script type="text/javascript">
    var videoObject = {
	container: '#video', //“#”代表容器的ID，“.”或“”代表容器的class
	variable: 'player', //播放函数名称，该属性必需设置，值等于下面的new ckplayer()的对象
	video: 'bailu.mp4'//视频地址
    };
    var player = new ckplayer(videoObject);
</script> 

<iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=450 src="//music.163.com/outchain/player?type=0&id=8376695998&auto=1&height=430"></iframe>
