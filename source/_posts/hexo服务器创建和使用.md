---
title: hexo服务器创建和使用
date: 2023-08-30 09:11:36
tags: [hexo,服务器]
categories: other
---
##### git的清理


执行  git rm * -r（记得，cd 到你要删除的目录下。当然 * 可以换成指定目录）

这时删除文件已经进入本地缓存区，
git add .
git commit -m "update"
git push origin branchName

或者直接 git push

#### hexo的插件

[文章教程](https://hexo.io/zh-cn/docs/tag-plugins#%E7%A4%BA%E4%BE%8B-1)

yarn   add          hexo-deployer-git
yarn add            hexo-abbrlink                        [断链使用](https://zhuanlan.zhihu.com/p/169492685)
yarn  add           hexo-spoiler                          模糊使用              {% spoiler style:blur 糊里糊涂 %}
yarn  add           markdown-it-ruby                 [上方注释使用yarn add            hexo-renderer-marked         ](https://loafing.cn/posts/hexo-tags.html#%E6%96%87%E5%AD%97%E4%B8%8A%E6%96%B9%E6%B3%A8%E9%87%8A-Ruby)[文档](https://github.com/hexojs/hexo-renderer-marked)
yarn add            hexo-generator-sitemap       [文档 ](https://www.dianjilingqu.com/249024.html)
yarn add            hexo-lazyload-image            [文档](https://blog.csdn.net/qq_37402392/article/details/127029330)
yarn  add           hexo-generator-searchdb     [文档](https://www.jianshu.com/p/fa09b1b51dde)
yarn add            hexo-filter-nofollow
yarn add            hexo-submit-urls-to-search-engine         [文档](https://cjh0613.com/20200603HexoSubmitUrlsToSearchEngine#%E7%99%BE%E5%BA%A6)
yarn  add           hexo-blog-encrypt               [文档](https://cloud.tencent.com/developer/article/1917942)
yarn add            hexo-tag-qrcode                  [文档](https://blog.jijian.link/2020-01-25/hexo-qrcode/)
yarn add             hexo-neat                         	[文档](https://loafing.cn/posts/hexo-tags.html#%E4%BB%A3%E7%A0%81%E5%8E%8B%E7%BC%A9)

[yarn add imagemin-jpegtran imagemin-svgo imagemin-gifsicle imagemin-optipng --dev](https://loafing.cn/posts/hexo-tags.html#%E4%BB%A3%E7%A0%81%E5%8E%8B%E7%BC%A9)

[教程]([https://hexo.io/zh-cn/docs/asset-folders](https://loafing.cn/posts/hexo-tags.html#%E4%BB%A3%E7%A0%81%E5%8E%8B%E7%BC%A9))

[教程2]([https://hexo.io/zh-cn/docs/tag-plugins#iframe](https://loafing.cn/posts/hexo-tags.html#%E4%BB%A3%E7%A0%81%E5%8E%8B%E7%BC%A9))

#### tips

相对路径引用的标签插件
通过常规的 markdown 语法和相对路径来引用图片和其它资源可能会导致它们在存档页或者主页上显示不正确。在Hexo 2时代，社区创建了很多插件来解决这个问题。但是，随着Hexo 3 的发布，许多新的标签插件被加入到了核心代码中。这使得你可以更简单地在文章中引用你的资源。

{  % asset_path slug %   }
{  % asset_img slug [title] %  }
{  % asset_link slug [title]  %  }

```
比如说：当你打开文章资源文件夹功能后，你把一个 example.jpg 图片放在了你的资源文件夹中，如果通过使用相对路径的常规 markdown 语法 ![img](example.jpg) ，它将 不会 出现在首页上。（但是它会在文章中按你期待的方式工作）
```

正确的引用图片方式是使用下列的标签插件而不是 markdown ：

{  %  asset_img example.jpg This is an example image %  }

```
通过这种方式，图片将会同时出现在文章和主页以及归档页中。
```
